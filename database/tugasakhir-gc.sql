-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 05, 2023 at 03:08 PM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tugasakhir-gc`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_kegiatan`
--

CREATE TABLE `jadwal_kegiatan` (
  `id` int(11) NOT NULL,
  `nama_kegiatan` varchar(100) NOT NULL,
  `jenis_kegiatan` varchar(100) NOT NULL,
  `tgl_mulai` date NOT NULL,
  `tgl_akhir` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `jadwal_kegiatan`
--

INSERT INTO `jadwal_kegiatan` (`id`, `nama_kegiatan`, `jenis_kegiatan`, `tgl_mulai`, `tgl_akhir`) VALUES
(1, 'Jalur Prestasi Khusus', 'Pendaftaran', '2022-12-18', '2023-05-31'),
(2, 'Jalur Prestasi Khusus', 'Wawancara', '2023-06-01', '2023-06-17'),
(3, 'Jalur Prestasi Khusus', 'Pengumuman', '2023-07-01', '2023-07-07'),
(4, 'Jalur Prestasi Khusus', 'Registrasi Ulang', '2023-07-10', '2023-07-15'),
(5, 'Jalur Reguler Gelombang 1', 'Pendaftaran', '2022-12-18', '2023-05-31'),
(6, 'Jalur Reguler Gelombang 1', 'Tes Potensi Akademik dan Wawancara', '2023-06-01', '2023-06-16'),
(7, 'Jalur Reguler Gelombang 1', 'Pengumuman', '2023-06-19', '2023-03-25'),
(8, 'Jalur Reguler Gelombang 1', 'Registrasi Ulang', '2023-03-19', '2023-06-17');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran`
--

CREATE TABLE `pembayaran` (
  `id_pembayaran` varchar(10) NOT NULL,
  `bukti_pembayaran` varchar(250) DEFAULT NULL,
  `status_pembayaran` varchar(30) NOT NULL,
  `tgl_pembayaran` timestamp NOT NULL DEFAULT current_timestamp(),
  `id_pendaftaran` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `pembayaran`
--

INSERT INTO `pembayaran` (`id_pembayaran`, `bukti_pembayaran`, `status_pembayaran`, `tgl_pembayaran`, `id_pendaftaran`) VALUES
('PAY0002', 'data pendaftar/202312003/payment-1642474501-banner (2).png', 'Dibayar', '2023-01-12 09:42:23', '202312003'),
('PAY0003', NULL, 'Gratis', '2023-01-12 14:10:52', '202312009'),
('PAY0004', NULL, 'Belum Bayar', '2023-05-05 13:05:00', '202305004');

-- --------------------------------------------------------

--
-- Table structure for table `pendaftaran`
--

CREATE TABLE `pendaftaran` (
  `id_pendaftaran` int(25) NOT NULL,
  `id_user` varchar(15) NOT NULL,
  `nisn` varchar(20) NOT NULL,
  `nik` varchar(20) NOT NULL,
  `nama_siswa` varchar(100) NOT NULL,
  `jenis_kelamin` varchar(25) NOT NULL,
  `pas_foto` varchar(500) NOT NULL,
  `tempat_lahir` varchar(25) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `agama` varchar(25) NOT NULL,
  `alamat` varchar(250) NOT NULL,
  `email` varchar(100) NOT NULL,
  `nohp` varchar(15) NOT NULL,
  `gelombang` varchar(500) NOT NULL,
  `tahun_masuk` year(4) NOT NULL,
  `pil1` varchar(100) NOT NULL,
  `pil2` varchar(100) NOT NULL,
  `nama_ayah` varchar(100) NOT NULL,
  `nama_ibu` varchar(100) NOT NULL,
  `pekerjaan_ayah` varchar(50) NOT NULL,
  `pekerjaan_ibu` varchar(50) NOT NULL,
  `nohp_ayah` varchar(15) NOT NULL,
  `nohp_ibu` varchar(15) NOT NULL,
  `gaji` varchar(50) NOT NULL,
  `tanggungan` int(5) NOT NULL,
  `slip_gaji` varchar(500) NOT NULL,
  `kk` varchar(500) NOT NULL,
  `id_Sekolah` varchar(50) NOT NULL,
  `jurusan` varchar(50) NOT NULL,
  `smt1` double NOT NULL,
  `smt2` double NOT NULL,
  `smt3` double NOT NULL,
  `smt4` double NOT NULL,
  `smt5` double NOT NULL,
  `nilairaport` varchar(500) NOT NULL,
  `ijazah` varchar(500) DEFAULT NULL,
  `prestasi` varchar(250) DEFAULT NULL,
  `status_pendaftaran` varchar(50) NOT NULL,
  `tgl_pendaftaran` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `pendaftaran`
--

INSERT INTO `pendaftaran` (`id_pendaftaran`, `id_user`, `nisn`, `nik`, `nama_siswa`, `jenis_kelamin`, `pas_foto`, `tempat_lahir`, `tanggal_lahir`, `agama`, `alamat`, `email`, `nohp`, `gelombang`, `tahun_masuk`, `pil1`, `pil2`, `nama_ayah`, `nama_ibu`, `pekerjaan_ayah`, `pekerjaan_ibu`, `nohp_ayah`, `nohp_ibu`, `gaji`, `tanggungan`, `slip_gaji`, `kk`, `id_Sekolah`, `jurusan`, `smt1`, `smt2`, `smt3`, `smt4`, `smt5`, `nilairaport`, `ijazah`, `prestasi`, `status_pendaftaran`, `tgl_pendaftaran`) VALUES
(202301009, 'USR000005', '3241231232', '464625432324', 'Nopi Rahmawati', 'Laki-laki', 'data pendaftar/202301009/Pasfoto1643709842-IMG_0620.JPG', 'Purwakarta', '2001-11-01', 'Islam', 'Purwakarta', 'nopiraa01@gmail.com', '085759933124', 'Jalur Reguler Gelombang 1', 2023, 'PR004', 'PR004', 'ayahku', 'ibuku', 'Wirausaha', 'PNS', '085759933124', '084348928343', '7.500.000 - 10.000.000', 32, 'data pendaftar/202301009/Slipgaji1643709842-PMB STT PEKANBARU.pdf', 'data pendaftar/202301009/KartuKeluarga1643709842-PMB STT PEKANBARU.pdf', '20227358', 'Agama', 45, 35, 56, 65, 67, 'data pendaftar/202301009/Pasfoto1643709842-PVisual Audio.zip', NULL, NULL, 'Selesai', '2023-02-01 10:04:03'),
(202302010, 'USR000027', '2313123234', '45343412312332', 'salsabila', 'Laki-laki', 'data pendaftar/202302010/Pasfoto1643768398-IMG_0617.JPG', 'purwakarta', '2000-01-01', 'Kristen', '0834657345', 'salsabila@gmail.com', '083236754254', 'Jalur Reguler Gelombang 1', 2023, 'PR003', 'PR003', 'Deris Nuryono Bimo', 'Iis Julaeha', 'Perawat', 'Wirausaha', '0892804837483', '0863254634434', '1.000.000 - 2.500.000', 4, 'data pendaftar/202302010/Slipgaji1643768399-PMB STT PEKANBARU.pdf', 'data pendaftar/202302010/KartuKeluarga1643768399-PMB STT PEKANBARU.pdf', '20227361', 'Agama', 98, 34, 54, 56, 76, 'data pendaftar/202302010/Pasfoto1643768399-1630670290_htmltopdf.zip', NULL, NULL, 'Selesai', '2023-02-02 02:19:59'),
(202305004, 'USR000031', '777777', '14711111', 'ucok', 'Laki-laki', 'data pendaftar/202305004/Pasfoto1683291899-69decb7a-4072-4e9b-a2e3-b5b88dea9551.jpg', 'Pelalawan', '2000-01-01', 'Islam', 'pesisir', 'ucok53@gmail.com', '0000000', 'Jalur Reguler Gelombang 1', 2022, 'PR001', 'PR002', 'bahrul ulum', 'Siti', 'PNS', 'PNS', '0000000', '0000000', '5.000.000 - 7.500.000', 2, 'data pendaftar/202305004/Slipgaji1683291899-5c2d6195-bec9-49af-aa3e-6fcf23839da3.jpg', 'data pendaftar/202305004/KartuKeluarga1683291899-4a7c7164-85ec-4757-8601-5f8d9a7ed1bd.jpg', '20227363', 'Tata Busana', 56, 56, 56, 56, 56, 'data pendaftar/202305004/Pasfoto1683291900-4a7c7164-85ec-4757-8601-5f8d9a7ed1bd.jpg', 'data pendaftar/202305004/Ijazah1683291900-4a7c7164-85ec-4757-8601-5f8d9a7ed1bd.jpg', 'data pendaftar/202305004/Prestasi1683291900-4a7c7164-85ec-4757-8601-5f8d9a7ed1bd.jpg', 'Belum Terverifikasi', '2023-05-05 13:05:00');

-- --------------------------------------------------------

--
-- Table structure for table `pengumuman`
--

CREATE TABLE `pengumuman` (
  `id_pengumuman` varchar(15) NOT NULL,
  `id_pendaftaran` int(15) NOT NULL,
  `hasil_seleksi` varchar(100) NOT NULL,
  `prodi_penerima` varchar(100) DEFAULT NULL,
  `nilai_interview` double DEFAULT NULL,
  `nilai_test` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `pengumuman`
--

INSERT INTO `pengumuman` (`id_pengumuman`, `id_pendaftaran`, `hasil_seleksi`, `prodi_penerima`, `nilai_interview`, `nilai_test`) VALUES
('ANN002', 202312003, 'LULUS', 'PR002', 95, 45),
('ANN003', 202312009, 'LULUS', 'PR004', 95, 95),
('ANN005', 202319005, 'TIDAK LULUS', 'PR001', 23, 23),
('ANN006', 202305004, 'Belum Seleksi', 'Belum Tersedia', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `profile_user`
--

CREATE TABLE `profile_user` (
  `Id_user` varchar(15) NOT NULL,
  `Nama` varchar(100) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Foto` varchar(250) DEFAULT NULL,
  `Tempat_lahir` varchar(50) DEFAULT NULL,
  `Tanggal_lahir` date NOT NULL,
  `Gender` varchar(25) DEFAULT NULL,
  `No_Hp` varchar(15) DEFAULT NULL,
  `Alamat` varchar(250) DEFAULT NULL,
  `Instagram` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `profile_user`
--

INSERT INTO `profile_user` (`Id_user`, `Nama`, `Email`, `Foto`, `Tempat_lahir`, `Tanggal_lahir`, `Gender`, `No_Hp`, `Alamat`, `Instagram`) VALUES
('USR000001', 'Luthfiyah Sakinah', 'luthfiyah.sakinah19@gmail.com', 'foto profil/1642312401-logo.png', 'Purwakarta', '2002-07-19', 'Perempuan', '085759933124', 'Jl Kapten Ismail RT 13/ RW 07 Sindangkasih Purwakarta. Jawa Barat 41112', 'piaaasan'),
('USR000030', 'bayu53', 'bayyu531@gmail.com', 'foto profil/1683288253-pngtree-happy-sunnah-family-illustration-of-married-brothers-and-sisters-cute-children-png-image_4377565.png', NULL, '2000-01-01', 'Laki-laki', '0834', NULL, NULL),
('USR000031', 'ucok', 'ucok@gmail.com', NULL, NULL, '2000-01-01', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `program_studi`
--

CREATE TABLE `program_studi` (
  `id_prodi` varchar(15) NOT NULL,
  `nama_prodi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `program_studi`
--

INSERT INTO `program_studi` (`id_prodi`, `nama_prodi`) VALUES
('PR001', 'TEKNIK ELEKTRO'),
('PR002', 'TEKNIK MESIN'),
('PR003', 'TEKNIK SIPIL'),
('PR004', 'SISTEM INFORMASI');

-- --------------------------------------------------------

--
-- Table structure for table `sekolah`
--

CREATE TABLE `sekolah` (
  `NPSN` varchar(25) NOT NULL,
  `nama_sekolah` varchar(100) NOT NULL,
  `alamat` varchar(250) NOT NULL,
  `kota` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `sekolah`
--

INSERT INTO `sekolah` (`NPSN`, `nama_sekolah`, `alamat`, `kota`) VALUES
('20227338', 'SMAN 1 PEKANBARU', 'LIHAT DI GOOGLE MAP', 'Pekanbaru'),
('20227357', 'SMAN 2 PEKANBARU', 'LIHAT DI GOOGLE MAP', 'Pekanbaru'),
('20227358', 'SMAN 3 PEKANBARU', 'LIHAT DI MAP', 'Pekanbaru'),
('20227361', 'SMAN 4 PEKANBARU', 'LIHAT DI MAP', 'Pekanbaru'),
('20227363', 'SMAN 5 PEKANBARU', 'LIHAT DI MAP', 'Pekanbaru'),
('20227461', 'SMAN 6 PEKANBARU', 'LIHAT DI MAP', 'Pekanbaru');

-- --------------------------------------------------------

--
-- Table structure for table `timeline`
--

CREATE TABLE `timeline` (
  `id` int(11) NOT NULL,
  `id_user` varchar(50) NOT NULL,
  `status` varchar(250) NOT NULL,
  `tgl_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `timeline`
--

INSERT INTO `timeline` (`id`, `id_user`, `status`, `tgl_update`) VALUES
(24, 'USR000001', 'Memperbaharui Pembayaran', '2023-01-18 09:55:01'),
(25, 'USR000001', 'Memperbaharui Pembayaran', '2023-01-18 09:56:09'),
(26, 'USR000001', 'Mengupdate pengumuman', '2023-01-19 09:02:36'),
(27, 'USR000001', 'Mengupdate pengumuman', '2023-01-19 09:03:20'),
(28, 'USR000001', 'Mengupdate pengumuman', '2023-01-19 09:11:00'),
(29, 'USR000001', 'Mengupdate pengumuman', '2023-01-19 09:11:48'),
(30, 'USR000001', 'Mengupdate pengumuman', '2023-01-19 09:12:34'),
(31, '202313009', 'di verifikasi', '2023-01-19 09:12:56'),
(32, 'USR000001', 'Mengupdate pengumuman', '2023-01-19 09:13:19'),
(33, 'USR000011', 'Bergabung', '2023-01-19 10:29:50'),
(34, 'USR000011', 'Melakukan pendaftaran penerimaan mahasiswa baru', '2023-01-19 10:39:55'),
(35, 'USR000012', 'Bergabung', '2023-01-19 10:41:50'),
(36, '202319005', 'belum di verifikasi', '2023-01-19 10:43:55'),
(37, '202319005', 'di verifikasi', '2023-01-19 10:44:04'),
(38, 'USR000011', 'Mengupload Pembayaran', '2023-01-19 11:05:27'),
(39, 'USR000011', 'Mengupload Pembayaran', '2023-01-19 12:06:12'),
(40, 'USR000011', 'Mengupload Pembayaran', '2023-01-19 12:07:28'),
(41, 'USR000011', 'Mengupload Pembayaran', '2023-01-19 12:12:00'),
(42, 'USR000011', 'Mengupload bukti pembayaran', '2023-01-19 19:49:32'),
(43, 'USR000011', 'Mengupload bukti pembayaran', '2023-01-19 19:59:05'),
(44, 'USR000001', 'Mengupdate pengumuman', '2023-01-20 16:39:53'),
(45, 'USR000006', 'Mengedit profilnya', '2023-01-20 21:34:36'),
(46, 'USR000013', 'Bergabung', '2023-01-20 22:37:24'),
(47, 'USR000013', 'Mengedit profilnya', '2023-01-20 22:40:37'),
(48, 'USR000013', 'Melakukan pendaftaran penerimaan mahasiswa baru', '2023-01-20 22:44:38'),
(49, '202320005', 'di verifikasi', '2023-01-20 22:46:52'),
(50, 'USR000001', 'Mengedit Pendaftaran', '2023-01-20 23:23:48'),
(51, 'USR000001', 'Mengedit Pendaftaran', '2023-01-20 23:27:29'),
(52, 'USR000014', 'Bergabung', '2023-01-20 23:54:46'),
(53, 'USR000014', 'Mengedit profilnya', '2023-01-20 23:58:45'),
(54, 'USR000014', 'Melakukan pendaftaran penerimaan mahasiswa baru', '2023-01-21 00:29:28'),
(55, '202320005', 'di verifikasi', '2023-01-21 00:31:27'),
(56, 'USR000015', 'Bergabung', '2023-01-21 07:50:00'),
(57, 'USR000015', 'Mengedit profilnya', '2023-01-21 08:00:33'),
(58, 'USR000015', 'Melakukan pendaftaran penerimaan mahasiswa baru', '2023-01-21 08:05:29'),
(59, '202321006', 'di verifikasi', '2023-01-21 08:08:39'),
(60, 'USR000015', 'Mengupload bukti pembayaran', '2023-01-21 08:10:24'),
(61, 'USR000001', 'Mengupdate pengumuman', '2023-01-21 08:17:45'),
(62, 'USR000016', 'Bergabung', '2023-01-25 16:00:43'),
(63, 'USR000017', 'Bergabung', '2023-01-25 16:03:28'),
(64, 'USR000018', 'Bergabung', '2023-01-25 16:06:37'),
(65, 'USR000019', 'Bergabung', '2023-01-25 16:12:21'),
(66, 'USR000020', 'Bergabung', '2023-01-25 16:19:17'),
(67, 'USR000021', 'Bergabung', '2023-01-25 16:19:54'),
(68, 'USR000022', 'Bergabung', '2023-01-25 16:22:27'),
(69, 'USR000023', 'Bergabung', '2023-01-25 16:24:28'),
(70, 'USR000020', 'Melakukan pendaftaran penerimaan mahasiswa baru', '2023-01-25 16:37:22'),
(71, '202325007', 'di verifikasi', '2023-01-25 16:42:25'),
(72, 'USR000020', 'Mengupload bukti pembayaran', '2023-01-25 16:43:29'),
(73, 'USR000001', 'Mengupdate pengumuman', '2023-01-25 16:47:08'),
(115, 'USR000001', 'Membuat user baru', '2023-05-05 19:04:13'),
(116, 'USR000031', 'Bergabung', '2023-05-05 19:54:42'),
(117, 'USR000031', 'Melakukan pendaftaran penerimaan mahasiswa baru', '2023-05-05 20:05:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(50) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id_user` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `role`, `remember_token`, `created_at`, `updated_at`, `id_user`) VALUES
(1, 'Bayu', 'Bayu53@gmail.com', NULL, '$2y$10$e4a1qeyQz0IUzSYUmP/xcOl3.Goc89fXz/0x8949GEFB.Wb069MSW', 'Administrator', NULL, '2023-04-30 05:50:23', '2023-04-30 22:36:55', 'USR000001'),
(2, 'bayu53', 'bayyu531@gmail.com', NULL, '$2y$10$1bKKclK7Yw.qak6.5oVwduUUz2uaDLzq1kKAyq1FxfQhGRMLH3Kz2', 'Administrator', NULL, '2023-05-05 05:04:13', '2023-05-05 05:04:13', 'USR000030'),
(44, 'ucok', 'ucok@gmail.com', NULL, '$2y$10$.PZBFlQPjrSTjmNLJB5pLusdcIiI0t9nK8Kx6XWr6rLBZwP.tLGqy', 'Calon Mahasiswa', NULL, '2023-05-05 05:54:42', '2023-05-05 05:54:42', 'USR000031');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `jadwal_kegiatan`
--
ALTER TABLE `jadwal_kegiatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD PRIMARY KEY (`id_pembayaran`),
  ADD UNIQUE KEY `id_pendaftaran` (`id_pendaftaran`);

--
-- Indexes for table `pendaftaran`
--
ALTER TABLE `pendaftaran`
  ADD PRIMARY KEY (`id_pendaftaran`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `pil1` (`pil1`,`pil2`,`id_Sekolah`);

--
-- Indexes for table `pengumuman`
--
ALTER TABLE `pengumuman`
  ADD PRIMARY KEY (`id_pengumuman`),
  ADD UNIQUE KEY `id_pendaftaran_2` (`id_pendaftaran`),
  ADD KEY `id_pendaftaran` (`id_pendaftaran`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `profile_user`
--
ALTER TABLE `profile_user`
  ADD PRIMARY KEY (`Id_user`),
  ADD UNIQUE KEY `Email` (`Email`);

--
-- Indexes for table `program_studi`
--
ALTER TABLE `program_studi`
  ADD PRIMARY KEY (`id_prodi`);

--
-- Indexes for table `sekolah`
--
ALTER TABLE `sekolah`
  ADD PRIMARY KEY (`NPSN`);

--
-- Indexes for table `timeline`
--
ALTER TABLE `timeline`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jadwal_kegiatan`
--
ALTER TABLE `jadwal_kegiatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `timeline`
--
ALTER TABLE `timeline`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=118;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
